package com.stadnyk;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String [][] matrix;
        matrix = new String[6][6];

        for (int i = 0; i < 6; i++) {
            for(int j = 0; j < 6; j++){
                if(i == 0){

                    matrix[i][j] = Integer.toString(j) + " | ";

                } else if(j == 0){

                    matrix[i][j] = Integer.toString(i) + " |";

                } else {
                    matrix[i][j] = " - |";
                }
            }
        }

        Random random = new Random();
        int column = random.nextInt(4) + 1;
        int line = random.nextInt(4) + 1;

        System.out.println("All set. Get ready to rumble!" + "x=" + column + "; y=" + line);

        Scanner in = new Scanner(System.in);


        while(true){

            int inputtedColumn = 0;
            int inputtedLine = 0;

            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++){
                    System.out.print( matrix[i][j] );
                }
                System.out.println();
            }

            while ( inputtedColumn < 1 || inputtedColumn > 5){
                System.out.println("Input column (from 1 to 5):");
                inputtedColumn = in.nextInt();
            }

            while (inputtedLine < 1 || inputtedLine > 5) {
                System.out.println("Input line (from 1 to 5):");
                inputtedLine = in.nextInt();
            }

            System.out.println(inputtedColumn + " " + inputtedLine);

            if( inputtedColumn == column && inputtedLine == line) {

                System.out.println("You have won!");
            } else {

                matrix[inputtedLine][inputtedColumn] = " * |";

            }

//            break;
        }
    }
}
