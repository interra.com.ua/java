package com.stadnyk;

public class GameField {

    String [][] matrix = new String[6][6];

    int randomColumn;
    int randomLine;
    int inputtedColumn = 0;
    int inputtedLine = 0;


    public void startSetMatrixValues(){
        for (int i = 0; i < 6; i++) {
            for(int j = 0; j < 6; j++){
                if(i == 0){

                    this.matrix[i][j] = Integer.toString(j) + " | ";

                } else if(j == 0){

                    this.matrix[i][j] = Integer.toString(i) + " |";

                } else {

                    this.matrix[i][j] = " - |";

                }
            }
        }
    }

    public void setValue(int column, int line, String value ) {
        this.matrix[column][line] = value;
    }

    public void printField(){

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++){
                System.out.print(this.matrix[i][j] );
            }
            System.out.println();
        }

    }


}
