package com.stadnyk;

import java.util.Random;
import java.util.Scanner;

public class GuessGame {

    GameField m1 = new GameField();
    Random random = new Random();
    Scanner in = new Scanner(System.in);


    public void startGame(){

        while (true){


            m1.startSetMatrixValues();

            m1.randomColumn = random.nextInt(4) + 1;
            m1.randomLine = random.nextInt(4) + 1;

            System.out.println("All set. Get ready to rumble!" + " randomColumn="+ m1.randomColumn + ", randomLine=" + m1.randomLine);

            while(true){

                m1.printField();

                System.out.println("Input column (from 1 to 5):");
                m1.inputtedColumn = in.nextInt();
                while ( m1.inputtedColumn < 1 || m1.inputtedColumn > 5){
                    System.out.println("Sorry, but column shoud be from 1 to 5. Please, enter again:");
                    m1.inputtedColumn = in.nextInt();
                }

                System.out.println("Input line (from 1 to 5):");
                m1.inputtedLine = in.nextInt();
                while ( m1.inputtedLine < 1 || m1.inputtedLine > 5){
                    System.out.println("Sorry, but line shoud be from 1 to 5. Please, enter again:");
                    m1.inputtedLine = in.nextInt();
                }

                System.out.println(m1.inputtedColumn + " " + m1.inputtedLine);

                if( m1.inputtedColumn == m1.randomColumn && m1.inputtedLine == m1.randomLine) {

                    System.out.println("You have won!");
                    m1.matrix[m1.inputtedLine][m1.inputtedColumn] = " x |";
                    m1.printField();
                    break;

                } else {

                    m1.matrix[m1.inputtedLine][m1.inputtedColumn] = " * |";

                }
            }

        }
    }





}
