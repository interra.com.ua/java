package hw5;

import java.util.Arrays;

class Family {
    private Human mother;
    private Human farther;
    private Human[] children = new Human[0];
    private Pet pet;

    Family(Human mother, Human farther) {
        this.mother = mother;
        this.farther = farther;
        mother.setFamily(this);
        farther.setFamily(this);

    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("Family{" +
                "mother=" + mother +
                ", farther=" + farther);
        if (children.length != 0){
            family.append(Arrays.toString(children));
        }
        if (pet != null){
            family.append(pet);
        }

        return family.toString();
    }

    Human getMother() {
        return mother;
    }

    Human getFarther() {
        return farther;
    }

    void setPet(Pet pet) {
        this.pet = pet;
    }

    Pet getPet() {
        return pet;
    }

    void addChild(Human child){
       Human [] array = new Human [children.length + 1];
        System.arraycopy(children, 0, array, 0, children.length);
       array[array.length - 1] = child;
       children = array;
       child.setFamily(this);
    }

    boolean deleteChild(int index){
        Human [] array = new Human [children.length - 1];
        for(int i = 0; i < array.length; i++){
            if(i < index){
                array[i] = children[i];
            } else {
                array[i] = children[i + 1];
            }
            children = array;
        }
        return true;
    }

    int countFamily(){
        return 2 + children.length;
    }
}
