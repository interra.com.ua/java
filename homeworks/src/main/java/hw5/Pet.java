package hw5;

import java.util.Arrays;
import java.util.Objects;

class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String [] habits;
    private Family family;

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(String species, String nickname, int age, int trickLevel, String... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        for(int i = 0; i < habits.length; i++){
            if (i == 0) {this.habits = new String[habits.length];}
            this.habits[i] = habits[i];
         }
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits, Family family) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.family = family;
    }

    @Override
    public String toString() {
        StringBuilder pet = new StringBuilder();
        if(!species.isEmpty()){
            pet.append(species + "{");
        } else {
            pet.append("{");
        }
        if(!nickname.isEmpty()){
            pet.append("nickname='" + nickname + "'");
        }

        if(age != 0){
            pet.append(", age=" + age);
        }
        if(trickLevel != 0){
            pet.append(", trickLevel=" + trickLevel);
        }
        if(habits != null){
            pet.append(", habits=" + Arrays.toString(habits));
        }
        pet.append("}");

        return pet.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return getSpecies().equals(pet.getSpecies()) &&
                getNickname().equals(pet.getNickname()) &&
                Objects.equals(getFamily(), pet.getFamily());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getNickname());
    }

    String getNickname() {
        return nickname;
    }

    String getSpecies() {
        return species;
    }

    int getAge() {
        return age;
    }

    int getTrickLevel() {
        return trickLevel;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    void eat(){
        System.out.println("Я кушаю!");
    }

    public void respond(){
        System.out.println("Привет, хозяин. Я - " + nickname + ". Я соскучился!");
    }

    public void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }
}
