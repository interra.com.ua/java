package hw5;


class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] shedule;
    private Family family;

    public Human() {
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    Human(String name, String surname, int year, int iq, String[][] shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.shedule = shedule;
    }

    @Override
    public String toString(){
        StringBuilder human = new StringBuilder();
        human.append("Human{");
        if(name != null){
            human.append("name='" + name + "'");
        } else human.append ("name='undefined'") ;
        if(surname != null){
            human.append(", surname='" + surname + "'");
        } else human.append (", surname='undefined'") ;
        if(year != 0){
            human.append(", year=" + year);
        }
        if(shedule != null){
            human.append(", shedule=" + getShedule());
        }
        human.append("}");

        return human.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human)obj;
        return (name.equals(human.name) && surname.equals(human.surname) && year == human.year);
    }

    @Override
    public int hashCode() {
        int code = 11;
        int k = 7;
        code = k * code + name.hashCode() + surname.hashCode() + year;
        return code;
    }

    public String getName() {
        return name;
    }


    void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public String getShedule() {
        String sheduleToString = "";

        for (int i = 0; i < shedule.length; i++){
            sheduleToString = sheduleToString + "[";
            for (int j = 0; j < shedule[0].length; j++){
                sheduleToString = sheduleToString + shedule[i][j];
                if(j == 0){
                    sheduleToString = sheduleToString + ", ";
                }
            }
            if (i != shedule.length -1) {
                sheduleToString = sheduleToString + "],";
            } else {
                sheduleToString = sheduleToString + "]";
            }
        }

        return sheduleToString;
    }

    void greatPet(){
        if(family != null && family.getPet() != null ) {
            System.out.println("Привет, " + family.getPet().getNickname());
        } else {
            System.out.printf("К сожалению, в нашей семье %s нет питомца и я не могу его поприветствовать %n", surname);
        }
    }

    void describePet(){
        if(family != null && family.getPet() != null ) {
            System.out.printf("У меня есть %s, ему %d лет, он %s.%n", family.getPet().getSpecies(), family.getPet().getAge(), ((family.getPet().getTrickLevel() > 50)?"очень хитрый" : "почти не хитрый"));
        } else {
            System.out.printf("К сожалению, в нашей семье %s нет питомца и я не могу его описать %n", surname);
        }

    }

    void feedPet(){
        if(family != null && family.getPet() != null){
            System.out.printf("Мой питомец %s, что ты делаешь? %n", family.getPet().getNickname());
            family.getPet().eat();
        } else {
            System.out.printf("К сожалению, в нашей семье %s нет питомца и я не могу его покормить %n", surname);
        }

    }




}
