package hw5;

class Main {
    public static void main(String[] args) {

        String[][] h = {{"Monday", "sleep"},{"Tuesday", "sleep"},{"Wednesday", "sleep"},{"Thursday", "sleep"},{"Friday", "sleep"},{"Saturday", "sleep"}, {"Sunday", "sleep"}};
        Pet beethoven = new Pet("dog", "Beethoven", 7, 85, "slobber", "save children", "sleep with family");

        Human zorro = new Human("Zorro", "de la Vega", 1970 ,85, h);
        Human wifeOfZorro = new Human("Elena", "de la Vega", 1975);
        Human daughterOfZorro = new Human("Alice", "de la Vega", 1997);
        Human sunOfZorro = new Human("Hoakin", "de la Vega", 2000, 86, h);

        Family deLaVega = new Family(wifeOfZorro, zorro);
        System.out.println(deLaVega.toString());

        deLaVega.setPet(beethoven);
        System.out.println(deLaVega.toString());

        deLaVega.addChild(daughterOfZorro);
        deLaVega.addChild(sunOfZorro);
        deLaVega.setPet(beethoven);
        System.out.println(deLaVega.toString());
        System.out.println("Количество членов семьи: " + deLaVega.countFamily());

        System.out.printf("%n ****** %n%n");

        deLaVega.deleteChild(0);
        System.out.println(deLaVega.toString());
        System.out.println("Количество членов семьи: " + deLaVega.countFamily());

        System.out.printf("%n ****** %n%n");

        zorro.greatPet();
        daughterOfZorro.describePet();
        wifeOfZorro.feedPet();

        System.out.printf("%n ****** %n%n");

        System.out.println("Zorro это Zorro? " + zorro.equals(zorro));
        System.out.println("Zorro это отец семьи de la Vega? " + zorro.equals(deLaVega.getFarther()));
        System.out.println("Отец семьи de la Vega - это Zorro? " + deLaVega.getFarther().equals(zorro));
        System.out.println("Является ли Zorro матерью семьи de la Vega? " + zorro.equals(deLaVega.getMother()));

    }
}
