package hw1;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int number;
        String name;

        Scanner in = new Scanner(System.in);
        Random random = new Random();

        System.out.print("Hello, what is your name?");
        name = in.nextLine();

        while (true){
            System.out.println("Hello, " + name + "! Let the game begin!");
            number = random.nextInt(100+1);
//            System.out.println(number);
            int[] inputedNumbers = new int[100];

            for( int i = 0; true; i++){
                System.out.println("Please, try to guess the number!");
                inputedNumbers[i] = in.nextInt();

                if( inputedNumbers[i] < number ){
                    System.out.println("Your number is too small. Please, try again.");
                } else if( inputedNumbers[i] > number){
                    System.out.println("Your number is too big. Please, try again.");
                } else if( inputedNumbers[i] == number ){
                    System.out.println("Congratulations, " + name + "! You won!!! \n \n \n");

                    for(int j = 0; j <= i; j++){
                        System.out.print(inputedNumbers[j] + " ");

                    }
                    System.out.println();
                    break;
                }

            }

        }
    }


}
