package hw4;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String [] habits;

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String ... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        for(int i = 0; i < habits.length; i++){
            if (i == 0) {this.habits = new String[habits.length];}
            this.habits[i] = habits[i];
         }


    }

    @Override
    public String toString() {
        StringBuilder pet = new StringBuilder();
        if(!species.isEmpty()){
            pet.append(species + "{");
        } else {
            pet.append("{");
        }
        if(!nickname.isEmpty()){
            pet.append("nickname='" + nickname + "'");
        }

        if(age != 0){
            pet.append(", age=" + age);
        }
        if(trickLevel != 0){
            pet.append(", trickLevel=" + trickLevel);
        }
        if(habits != null){
            pet.append(", habits=" + Arrays.toString(habits));
        }
        pet.append("}");

        return pet.toString();
//        return species + "{" + "nickname='" + nickname + "', age=" + age + ", trickLevel=" + trickLevel + ", habits=" + Arrays.toString(habits) + "}";
    }

    public String getNickname() {
        return nickname;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void eat(){
        System.out.println("Я кушаю!");
    }

    public void respond(){
        System.out.println("Привет, хозяин. Я - " + nickname + ". Я соскучился!");
    }

    public void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }
}
