package hw4;



public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human farther;
    private String[][] shedule;

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human farther) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.farther = farther;
    }


    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human farther, String[][] shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.farther = farther;
        this.shedule = shedule;
    }

    @Override
    public String toString(){
        StringBuilder human = new StringBuilder();
        human.append("Human{");
        if(name != null){
            human.append("name='" + name + "'");
        } else human.append ("name='undefined'") ;
        if(surname != null){
            human.append(", surname='" + surname + "'");
        } else human.append (", surname='undefined'") ;
        if(year != 0){
            human.append(", year=" + year);
        }
        if(iq != 0){
            human.append(", iq=" + iq);
        }
        if(mother != null){
            human.append(", mother=" + mother.name + " " + mother.surname);
        }
        if(farther != null){
            human.append(", farther=" + farther.name + " " + farther.surname);
        }
        if(pet != null){
            human.append(", pet=" + pet.toString());
        }
        human.append("}");

        return human.toString();
//         return "Human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + ", mother=" + mother +", father=" + farther + ", pet=" + pet.toString();
    }

    public String getName() {
        return name;
    }

    public void greatPet(){
        System.out.println("Привет, " + pet.getNickname());
    }

    public void describePet(){
        System.out.printf("У меня есть %s, ему %d лет, он %s.%n", pet.getSpecies(), pet.getAge(), ((pet.getTrickLevel() > 50)?"очень хитрый" : "почти не хитрый"));
    }



}
