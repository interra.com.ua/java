package hw4;

public class Main {
    public static void main(String[] args) {

        String[][] h = new String[7][2];
        Pet strayCat = new Pet();
        Pet fish = new Pet("fish","Dori");
        Pet beethoven = new Pet("dog", "Beethoven", 7, 85, "slobber", "save children", "sleep with family");

        Human zorro = new Human();
        Human wifeOfZorro = new Human("Elena", "de la Vega", 1975);
        Human daughterOfZorro = new Human("Alice", "de la Vega", 1997,  wifeOfZorro, zorro);
        Human sunOfZorro = new Human("Hoakin", "de la Vega", 2000, 86, beethoven, wifeOfZorro, zorro, h);

//        System.out.println(zorro.toString());
        System.out.println(zorro.toString());
        System.out.println(wifeOfZorro.toString());
        System.out.println(daughterOfZorro.toString());
        System.out.println(sunOfZorro.toString());

        System.out.printf("%n%20s%n%n", "**********");


        Pet pushok = new Pet("cat", "Pushok", 5, 50, "eat", "sleep", "repeat");
        String [][] s = new String[7][2];
        Human mother = new Human("Katya", "Pupkina", 1963);
        Human farther = new Human("Sanya", "Pupkin", 1961);
        Human vasya = new Human("Vasya", "Pupkin", 1985, 30, pushok, mother, farther, s);

        System.out.println(vasya.toString());
        vasya.describePet();
        vasya.greatPet();

        pushok.respond();
        pushok.eat();
        pushok.foul();

    }
}
