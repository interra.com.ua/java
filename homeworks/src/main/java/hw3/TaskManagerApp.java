package hw3;

public class TaskManagerApp {


    public static void main(String[] args) {

        TaskManager taskManager = new TaskManager();
        String usersAnswer;
        String inputedCommand;
        String inputedDay;



        taskManager.createTaskManager();


        label:
        do {
            usersAnswer = taskManager.askUser("Please, input the day of the week:");
            inputedCommand = taskManager.parseInputedCommand(usersAnswer);
            inputedDay = taskManager.parseInputedDay(usersAnswer);

            switch (inputedCommand) {
                case "Exit":
                    break label;
                case "Change":
                    String newTask = taskManager.askUser("Please, input new tasks for " + inputedDay + ".");
                    taskManager.setTask(inputedDay, newTask);

                    break;
                case "ReadTask":
                    if ("Mistake".equals(inputedDay)) {
                        System.out.println("You have inputed incorrect day name");
                        continue;
                    } else {
                        System.out.println("Your tasks for " + inputedDay + ": " + taskManager.getTask(inputedDay));
                    }
                    break;
            }

        } while (!inputedCommand.equalsIgnoreCase("Exit"));
    }
}
