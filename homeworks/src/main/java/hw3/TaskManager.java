package hw3;

import java.util.Scanner;

public class TaskManager {

    private String[][] scedule = new String[7][2];

    public void createTaskManager(){
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do homework";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to zoo;";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to theater;";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to cinema;";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to forest with friends;";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "stay home;";

    }

    public String askUser(String ask){
        Scanner in = new Scanner(System.in);
        System.out.println(ask);
        return in.nextLine();
    }

    public String parseInputedCommand(String value) {

        if (value.toLowerCase().contains("exit".toLowerCase())) {
            return "Exit";
        }else if (value.toLowerCase().contains("change".toLowerCase()) || value.toLowerCase().contains("reschedule")) {
            return "Change";
        } else return "ReadTask";
    }

    public String parseInputedDay(String value){

        for (String[] day : scedule) {
            if (value.toLowerCase().contains(day[0].toLowerCase())) {
                return day[0];
            }
        }

        return "Mistake";
    }

    public void setTask(String day, String task) {
        switch (day) {
            case ("Sunday"):
                scedule[0][1] = task;
                break;
            case ("Monday"):
                scedule[1][1] = task;
                break;
            case ("Tuesday"):
                scedule[2][1] = task;
                break;
            case ("Wednesday"):
                scedule[3][1] = task;
                break;
            case ("Thursday"):
                scedule[4][1] = task;
                break;
            case ("Friday"):
                scedule[5][1] = task;
                break;
            case ("Saturday"):
                scedule[6][1] = task;
                break;

            default:
                System.out.println("You have inputed incorrect day name");
        }
    }

    public String getTask(String day) {
        switch (day) {
            case ("Sunday"):
                return scedule[0][1];
            case ("Monday"):
                return scedule[1][1];
            case ("Tuesday"):
                return scedule[2][1];
            case ("Wednesday"):
                return scedule[3][1];
            case ("Thursday"):
                return scedule[4][1];
            case ("Friday"):
                return scedule[5][1];
            case ("Saturday"):
                return scedule[6][1];

            default:
                return "You have inputed incorrect day name";

        }
    }
}
