package hw2;

import java.util.Random;
import java.util.Scanner;

public class App {
    Scanner in = new Scanner(System.in);
    Random random = new Random();

    private int randomTargetLine;
    private int randomTargetColumn;
    private int targetLine;
    private int targetColumn;


    public void setRandomTarget() {
        System.out.print("Service info for testing: ");
        this.randomTargetLine = random.nextInt(5);
        System.out.print("randomTargetLine = " + (this.randomTargetLine + 1));
        this.randomTargetColumn = random.nextInt(5);
        System.out.println(", randomTargetColumn = " + (this.randomTargetColumn + 1) + "\n");
    }

    public void askTargetLineAndColumn(){
        int usersAnswer;

        do{
            System.out.println("Please, enter the line number from 1 to 5");
            usersAnswer = in.nextInt();
        } while(usersAnswer < 1 || usersAnswer > 5);

        targetLine = usersAnswer - 1;
        System.out.println("ура");

        do{
            System.out.println("Please, enter the column number from 1 to 5");
            usersAnswer = in.nextInt();
        } while(usersAnswer < 1 || usersAnswer > 5);
        targetColumn = usersAnswer - 1;
    }

    public int getRandomTargetLine(){
        return randomTargetLine;
    }

    public int getRandomTargetColumn(){
        return randomTargetColumn;
    }

    public int getTargetLine(){
        return targetLine;
    }

    public int getTargetColumn(){
        return targetColumn;
    }

    public boolean isGameOver(){
        return randomTargetLine != targetLine || randomTargetColumn != targetColumn;
    }

}
