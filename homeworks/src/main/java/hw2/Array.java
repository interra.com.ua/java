package hw2;

public class Array {

    private String[][] array;

    Array(int line, int col){
        this.array = new String [line][col];
    }

    public void setValue(int line, int col, String value){
        this.array[line][col] = value;
    }

    public void fillClearArray(){
        for(int line = 0; line < array.length; line++){
            for(int col = 0; col < array[0].length; col++){
                this.array[line][col] = "-|";
            }
        }
    }

    public void printArray (){
        for(int line = -1; line < array.length; line++){
            for(int col = -1; col < array[0].length; col++)
                if (line == -1) {
                    System.out.print(col + 1 + "|");
                } else if (col == -1) {
                    System.out.print(line + 1 + "|");
                } else {
                    System.out.print(array[line][col]);
                }
            System.out.println();
        }
    }
}
