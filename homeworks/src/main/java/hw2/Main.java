package hw2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Array array = new Array(5, 5);
        App app = new App();

        while(true){

            array.fillClearArray();
            app.setRandomTarget();
            System.out.println("All set. Get ready to rumble!");
            array.printArray();

            while( app.isGameOver()){
                app.askTargetLineAndColumn();
                if(app.isGameOver()){
                    array.setValue(app.getTargetLine(),app.getTargetColumn(), "*|");
                } else {
                    array.setValue(app.getTargetLine(),app.getTargetColumn(), "X|");
                }
                array.printArray();
            }

            System.out.println("You have won! \n");
        }
    }
}
