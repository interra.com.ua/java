package com.stadnyk;

import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        Random random = new Random();
        System.out.print("Input your name:");
        String name = in.nextLine();
        System.out.println("Hello, " + name +"!");

        while(true){
            System.out.println("Let the game begin!");
            int mindedNumber = random.nextInt(100);

            System.out.println("Input number: ");
            int number = in.nextInt();

            while(mindedNumber != number) {
                System.out.println(mindedNumber + "  " + number);

            if (number < mindedNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else {
                System.out.println("Your number is too big. Please, try again.");
            }

            System.out.println("Input number: ");
            number = in.nextInt();

            }

            System.out.println("Congratulations, " + name);
        }

    }
}
